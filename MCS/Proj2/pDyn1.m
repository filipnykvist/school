%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   1 Population Dynamics  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all

% plotting options
lyop = 0;
phase = 1;
func = 0;

% recource sites
noSites = 1000;

% simulation variables
T = 400;
TL = 10000;
bPhase = 40;
B = zeros(bPhase,T);

% initial condition
A_0 = 1000;
A = zeros(T,bPhase);
A(1,1:bPhase) = A_0;

% interaction function
%   b if k == 2
%   0 if otherwise 



if (phase)
    pop = scramble(bPhase,T,A,noSites);
    M = max(max(pop));
    for b = 1:bPhase
        histo(b,:) = hist(pop(200:end,b),0:50:M);
    end
    imagesc(histo', [0 50])
end
if (lyop)
    popMF = scrambleMFM(bPhase,TL,A,noSites);
    L = lyapunov(bPhase,TL,popMF,noSites);
    plot(L)
end
if (func)
    i = 1;
    x = 0:1:10000;
    for b = [5 10 20 40]
        n = noSites;
        y = (b*x.^2.*exp(-x./n))./(2*n);
        l = (x);
        subplot(2,2,i) 
        plot(y)
        hold on 
        plot(l)
        i = i + 1;
    end
end
