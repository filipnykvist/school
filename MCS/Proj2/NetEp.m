%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   3 Network Epidemics    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% adjacency matrix A_ij
%   j = 1 .. n --> Destination
%   i = 1 .. n --> Source
clear all
close all

histo = 0;
netInfec = 0;
social = 1;
socialInfec = 1;

n = 5000;
A = zeros(n,n);
k = 0;
in = zeros(n,1);

% link density  
l = 0.0016;

for i=2:n
    for j=i+1:n
        if (rand < l)
            A(i,j) = 1;
            A(j,i) = 1;
            in(j) = in(j) + 1;
            in(i) = in(i) + 1; 
        end
    end
end
out = in';
if (histo)
    hist(in);
    %hist(out)
    %k = (1/n^2)*sum(sum(A))
end

if (netInfec)
    %indices = randperm(n);
    %indices = indices(1:100);
    indices = 1:100;
    p = 0.001:0.001:0.01;
    %p = 0.01;
    r = 0.03;
    tStop = 300;
    for q = p
        infected = infecNet(indices,A,q,r,tStop,n);
        plot(infected)
        hold on 
    end 
end

if (social)
    nmax = 5000;
    B = zeros(nmax,nmax);
    B(1,2) = 1;
    B(2,1) = 1;
    B = socNet(B,nmax);
    %loglog(hist(sum(B)))
    if (socialInfec)
        indices = randperm(nmax);
        indices = indices(1:100);
        p = 0.01;
        r = 0.03;
        tStop = 300;
        for q = p
            infected = infecNet(indices,B,q,r,tStop,n);
            plot(infected)
            hold on 
        end
    end

end 
