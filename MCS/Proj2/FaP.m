%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   4 Flocks and Predators   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all

% plot
fig=figure;
makemovie=1;

% Variables
N = 40;     % particles
L = 10;     % daomain
e = 0.5;    % angular noise
T = 200;      % timesteps
v = 0.5;    % speed
%t = 1/J;

% parameters
n = 4;
F = 0;

% initial conditions
x = zeros(N,T+1);
x(:,1)=L*rand(N,1);

y = zeros(N,T+1);
y(:,1)=L*rand(N,1);

a=zeros(N,T+1);
a(:,1)=2*pi*rand(N,1);

% Predators -- set up owns rules
%   create 
predx = L*rand;
predy = L*rand;
% n config 
ind = randperm(40);
nLarge = ind(1:20);
nSmall = ind(21:40);

for t = 1:T
    comX = sum(x(:,t))/N;
    comY = sum(y(:,t))/N;
    for i = 1:N
        % identify n nearest neighbours
        dist = zeros(N,1);
        index = zeros(n,1);
        for j = 1:N
            dist(j) = sqrt((x(i,t)-x(j,t))^2 + (y(i,t)-y(j,t))^2);
        end
        sortdist = sort(dist);
        nearest = sortdist(2:n+1);
        for p = 1:n
            index(p) = find(nearest(p) == dist,1);
        end
        
        % new trajectory 
        ss=sum(sin(a(index,t)));
        sc=sum(cos(a(index,t)));
        S=atan2(ss,sc);

        a(i,t+1)=S+e*(rand-0.5);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        % small force to CoM
        CoMx = x(i,t) - comX;
        CoMy = y(i,t) - comY;
        CoMdir = atan2(CoMy,CoMx);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
    
        %updates the particles coord
        %x(i,t+1)=x(i,t)+v*cos(a(i,t+1)); 
        %y(i,t+1)=y(i,t)+v*sin(a(i,t+1)); 
        
        %updates the particles coord -- CoM
        x(i,t+1)=x(i,t)+v*cos(a(i,t+1))+F*cos(CoMdir); 
        y(i,t+1)=y(i,t)+v*sin(a(i,t+1))+F*sin(CoMdir);
        
        % continuous boundary
        x(i,t+1)=mod(x(i,t+1),L);
        y(i,j+1)=mod(y(i,t+1),L);
        
        % plotting
        if makemovie
            if abs(x(i,t)-x(i,t+1))<v && abs(y(i,t)-y(i,t+1))<v
                plot([x(i,t), x(i,t+1)], ...
                    [y(i,t),y(i,t+1)],'k-','markersize',4) %plots the first half of the particles in black
                axis([0 L 0 L]);
                hold on
                plot(x(i,t+1) ,y(i,t+1),'k.','markersize',10)
                xlabel('X position')
                ylabel('Y position')
            end
        end
    end
    if makemovie
        hold off
        M(j)=getframe; %makes a movie fram from the plot
        %movien = addframe(movien,M(j)); %adds this movie fram to the movie
    end
    % calculate global alignment
end