function freq = gFreq(n,r,T,noSims)
    freq = zeros(noSims,n);
    groups = ones(n,1);
    for p = 1:noSims
        for t = 2:T
            i = randi([1 length(groups)]);
            if (groups(i) == 1)
                j = randi([1 n]);
                k = find(j<=cumsum(groups),1);
                groups(k) = groups(k) + 1;
                groups(i) = groups(i) - 1;
                groups = groups(groups ~= 0);
            else
                if (rand < groups(i)*r)
                    for j=1:groups(i)-1
                        groups(end+1) = 1;
                    end
                    groups(i) = 1;
                end
            end
        end
%         for f=1:length(groups)
%             freq(groups(f)) = freq(groups(f)) + 1;
%         end
        freq(p,:) = freq(p,:) + hist(groups,1:100);
    end
    freq = freq./noSims;
end

    