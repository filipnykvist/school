function L = lyapunov(bPhase,T,A,noSites)
    L = zeros(bPhase,1);
    for b = 1:bPhase
        for i = 1:T
            e = exp(-A(i,b)/noSites);
            f = A(i,b)/noSites;
            g = (A(i,b)^2)/(2*noSites*noSites);
            B = 0;
            if (abs(b*e*(f - g)) > 1e-9)
                B = log(abs(b*e*(f - g)));
            end
            L(b) =  L(b) + B;
        end
    end
    L = L./T;
end

% function L = lyapunov(bPhase,T,A,n)
%     L = zeros(bPhase,1);
%     for b = 1:bPhase
%         for i = 1:T-1
%             B=0;
%             if (abs(A(i+1,b)-A(i,b))> 1e-8)
%                 B = log(abs((A(i+1,b)-A(i,b))));
%             end
%             L(b) =  L(b) + B;
%         end
%     end
%     L = L./T;
% end 