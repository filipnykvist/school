function infected = infecNet(indices,A,p,r,tStop,n)
infected(1) = 100;
for t = 2:tStop
    disp (t)
    for j = 1:n
        if (ismember(j,indices))
            if (rand < r)
                ind = find(indices >= j,1);
                indices(ind) = [];
            end
        else 
            a = 0;
            for k = 1:length(indices)
                a = a + A(j,indices(k));
            end
            if (rand < (1-exp(-p*a)))
                indices(end+1) = j;
            end
        end
    end
    infected(t)= length(indices);
end
end