function A = scrambleMFM(bPhase,T,A,noSites)
    for b = 1:bPhase
        disp(b)
        for i = 1:T-1
            A(i+1,b) = ((A(i,b)^2)*exp(-A(i,b)/noSites)*b)/(2*noSites);
        end
    end
end