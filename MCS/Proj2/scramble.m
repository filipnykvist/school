function A = scramble(bPhase,T,A,noSites)
    for b = 1:bPhase
        disp(b)
        for i = 2:T
            n = zeros(noSites,1);
            for j = 1:A(i-1,b)
                r = randi([1 noSites]);
                n(r) = n(r) + 1;  
            end
            A(i,b) = b*sum(n(:) == 2);
        end
    end   
end