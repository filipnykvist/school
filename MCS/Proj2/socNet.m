function [A] = socNet(A,nmax)
    for n = 3:nmax
        disp(n)
        deg = cumsum(sum(A(:,1:n-1)));
        con = randi([1 2*(n-2)]);
        conn = find(con <= deg,1);
        A(conn,n) = A(conn,n) + 1;
        A(n,conn) = A(n,conn) + 1;
    end
end