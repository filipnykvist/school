/**********************************************************************
 * Serial Quick sort
 * You can use this for Assignment 2
 **********************************************************************/
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

void swap(double *v, int i, int j)
{
    double t;
    t = v[i];
    v[i] = v[j];
    v[j] = t;
}

void quicksort(double *v, int left, int right)
{
    int i,last;
    if(left>=right)
        return;
    swap(v,left,(left+right)/2);
    last = left;
    for(i=left+1;i<=right;i++)
        if(v[i]<v[left])
            swap(v,++last,i);
    swap(v,left,last);
    quicksort(v,left,last-1);
    quicksort(v,last+1,right);
}

void merge(double *a, double *b, double *locdata, int alen, int blen)
{
	int i=0,j=0,k=0;
	while (i<alen && j<blen){
		if (a[i]<b[j]){
			locdata[k] = a[i];
			i++;
		} else {
			locdata[k] = b[j];
			j++;
		}
		k++;
	}
	if (i>=alen-1){
		while (j<blen) {
			locdata[k] = b[j];
			j++;
			k++;
		}
	}
	if (j>=blen-1){
		while (i<alen) {
			locdata[k] = a[i];
			i++;
			k++;
		}
	}
}

/*
double getpivot(int len, double *locdata)
{
	double pivot = locdata[(int)(len/2)];
	printf("pivot: %f len: %d\n",pivot,len);
	return pivot;
}*/


void pquick(double * locdata, int len, MPI_Comm Comm)
{	
	double pivot;
	int i,j,size,rank,grank,length;
	MPI_Status stat, rec;
	MPI_Request req;
	MPI_Comm newcomm;

	MPI_Comm_size(Comm,&size); 
	MPI_Comm_rank(Comm,&rank);
	printf("size: %d rank: %d\n",size, rank);

	if (size == 1) {
		MPI_Comm_rank(MPI_COMM_WORLD,&grank);
		/*for (i=0;i<len;i++)
			printf("%d %f ",grank, locdata[i]);
		printf("\n");*/
		MPI_Isend(locdata,len,MPI_DOUBLE,0,grank,MPI_COMM_WORLD,&req);
		return;
	}
	/* pivot */
	if (rank == 0) 
		pivot = locdata[(int)(len/2)];
	//pivot = getpivot(len,locdata);
	MPI_Bcast(&pivot,1,MPI_DOUBLE,0,Comm);
	MPI_Barrier(Comm);
	
	
	/* Split array in two */
	int split = 0;
	while (locdata[split]<=pivot) split++;  
	double low[split];
	double high[(len-split)];
	for (i=0;i<split;i++)
		low[i] = locdata[i];
	j = 0;
	for (i=split;i<len;i++){
		high[j] = locdata[i];
		j++;
	}

	/* exchange data and merge*/
	int m_size = 0;
	int keepLower = (rank<=((size-1)/2));
	
	if (keepLower) {
		MPI_Isend(&high,(len-split),MPI_DOUBLE,
					rank + ((size)/2),444,Comm,&req);
		
		MPI_Probe(MPI_ANY_SOURCE,555,Comm,&rec);
		MPI_Get_count(&rec,MPI_DOUBLE,&m_size);
		
		double tmp[m_size];
		MPI_Recv(&tmp,m_size,MPI_DOUBLE,
					rank + ((size)/2),555,Comm,&stat);
		
		/* merge arrays */ 
		length = split + m_size;
		locdata = (double*)realloc(locdata, (length)*sizeof(double)); 
		merge(low,tmp,locdata,split,m_size);
	} else {
		MPI_Isend(&low,split,MPI_DOUBLE,
					rank - ((size)/2),555,Comm,&req);
		
		MPI_Probe(MPI_ANY_SOURCE,444,Comm,&rec);
		MPI_Get_count(&rec,MPI_DOUBLE,&m_size);
		
		double tmp[m_size];
		MPI_Recv(&tmp,m_size,MPI_DOUBLE,
					rank - ((size)/2),444,Comm,&stat);
		/* merge arrays */ 
		length = (len-split) + m_size;
		locdata = (double*)realloc(locdata, (length)*sizeof(double)); 
		merge(high,tmp,locdata,(len - split),m_size);
	}
	

	len = length;
	// Comm split
	if (rank < (int)((size)/2)) 
		MPI_Comm_split(Comm, 0, 0,&newcomm);
	else 
		MPI_Comm_split(Comm, 1, 0,&newcomm);
	pquick(locdata,len,newcomm);
}




int main (int argc, char *argv[]) {
  	double *data;
  	double *locdata;
  	int i,j,counter,len,m_size;
  	int size,rank,chunk_size;
  	MPI_Request req;
  	MPI_Status stat;

  	MPI_Init(&argc, &argv);
  	/***********************************************/
  	MPI_Comm_size(MPI_COMM_WORLD,&size); 
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  	if (rank == 0)
  	{
	  	printf("Give number of elements: \n");
	  	scanf("%d",&len);
	  	data=(double*)malloc(len*sizeof(double));
	  	// Generate random numbers on P_0
	   	for (i=0;i<len;i++)
	    	data[i]=drand48();
	}
	/***********************************************/
	MPI_Bcast(&len,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	/***********************************************/
	if (rank == 0){
		chunk_size = (int)(len/size);
		int last_chunk = (int)chunk_size + len%size; 
		double buffer[chunk_size];
		double lBuffer[last_chunk];
		for (j=0;j<size;j++){
			if (j != size-1){
				for (i=0;i<chunk_size;i++)
					buffer[i] = data[j*chunk_size + i];
				MPI_Isend(&buffer,chunk_size,MPI_DOUBLE,j,000,MPI_COMM_WORLD,&req);
			} else {
				for (i=0;i<last_chunk;i++)
					lBuffer[i] = data[j*chunk_size + i];
				MPI_Isend(&lBuffer,last_chunk,MPI_DOUBLE,j,000,MPI_COMM_WORLD,&req);
			}		
		}
	}
	if (rank == size-1){
		chunk_size = len/size + len%size;
		locdata = (double*)malloc((chunk_size)*sizeof(double));
		MPI_Recv(locdata,chunk_size,MPI_DOUBLE,0,
				000,MPI_COMM_WORLD,&stat);
	} else {
		chunk_size = len/size;
		locdata = (double*)malloc((chunk_size)*sizeof(double));
		MPI_Recv(locdata,chunk_size,MPI_DOUBLE,0,000,MPI_COMM_WORLD,&stat);
	}
	MPI_Barrier(MPI_COMM_WORLD);

	/***********************************************/
  	// Locally Sort
    quicksort(locdata,0,chunk_size-1);
    
    pquick(locdata,chunk_size,MPI_COMM_WORLD);
    /***********************************************/

	if (rank==0){
		counter = 0;
		for (i=0;i<size;i++){
	    	MPI_Probe(i,i,MPI_COMM_WORLD,&stat);
	    	MPI_Get_count(&stat,MPI_DOUBLE,&m_size);
	    	double buffer[m_size];
			MPI_Recv(&buffer,m_size,MPI_DOUBLE,i,i,MPI_COMM_WORLD,&stat);
			for (j=0;j<m_size;j++){
				data[counter] = buffer[j];
				counter++;
			}
	    }
	    for (i=0;i<len;i++)
			printf("%f ", data[i]);
		printf("\n");
	}
	
    /***********************************************/
    //free();
    /***********************************************/
	MPI_Finalize();
 	return 0;
}
